<?php

namespace App\Http\Middleware;

use Closure;

class AuthJwt
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        try {
            $user = $this->guard()->userOrFail();
        } catch (\Tymon\JWTAuth\Exceptions\UserNotDefinedException $e) {
            echo $e->getMessage();
        }
        return $next($request);
    }

    public function guard()
    {
        return Auth::guard('api');
    }
}
