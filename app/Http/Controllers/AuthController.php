<?php
namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use Tymon\JWTAuth\Facades\JWTAuth;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;


class AuthController extends Controller
{

    public function register(Request $request)
    {
        $v = Validator::make($request->all(), [
            'email' => 'required|email|unique:users',
            'name' => 'required|string|max:255',
            'password'  => 'required|min:8|confirmed',
        ]);

        if ($v->fails())
        {
            return response()->json([
                'status' => 'error',
                'errors' => $v->errors()
            ], 422);
        }

        $user = new User;
        $user->email = $request->email;
        $user->name = $request->name;
        $user->password = bcrypt($request->password);
        $user->save();
        $token = JWTAuth::fromUser($user);

        return response()->json(['status' => 'success','Token' => $token], 200);
    }

    public function login(Request $request)
    {
        $credentials = request(['email', 'password']);
        $user = User::where(['email' => $request->email])->first();
    
        if($user->confrimed == 1)
        {
            if (! $token = auth('api')->attempt($credentials)) {
                return response()->json(['error' => 'Invalid Credentials'], 401);
            }

                return $this->respondWithToken($token);
        }
        else
        {
            $token = JWTAuth::fromUser($user);
            return response()->json(['status' => 'success','Token' => $token], 200);
        }            
    }

        /**
         * Get the authenticated User
         *
         * @return \Illuminate\Http\JsonResponse
         */
    public function getUser()
    {
        return response()->json($this->guard()->user());
    }

        /**
         * Log the user out (Invalidate the token)
         *
         * @return \Illuminate\Http\JsonResponse
         */
    public function logout()
    {
    $this->guard()->logout();

        return response()->json(['message' => 'Successfully logged out']);
    }

        /**
         * Refresh a token.
         *
         * @return \Illuminate\Http\JsonResponse
         */
    public function refresh()
    {
        return $this->respondWithToken($this->guard('api')->refresh());
    }

        /**
         * Get the token array structure.
         *
         * @param string $token
         *
         * @return \Illuminate\Http\JsonResponse
         */

    protected function respondWithToken($token)
    {
        return response()->json([
            'access_token' => $token,
            'token_type' => 'bearer',
            'expires_in' => $this->guard('api')->factory()->getTTL() * 60
        ]);
    }

        /**
         * Get the guard to be used during authentication.
         *
         * @return \Illuminate\Contracts\Auth\Guard
         */

    public function guard()
    {
        return Auth::guard('api');
    }
    
}

