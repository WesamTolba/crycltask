<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Route::middleware('auth:api')->get('/user', function (Request $request) {
//     return $request->user();
// });

Route::prefix('auth')->group(function () {
    // Register with JWT
    Route::post('register', 'AuthController@register');
    // Login with JWT
    Route::post('login', 'AuthController@login');
    // Refresh JWTtoken
    Route::get('refresh', 'AuthController@refresh');


    Route::group(['middleware' => 'auth:api'], function(){
        // get user
        Route::get('user', 'AuthController@getUser');
        // logout
        Route::post('logout', 'AuthController@logout');

        // employees
        Route::get('employees', 'EmployeeController@index');
        Route::post('employees', 'EmployeeController@store');
        Route::post('employees/{employee}', 'EmployeeController@update');
        Route::get('employees/{employee}', 'EmployeeController@show');
        Route::delete('employees/{employee}', 'EmployeeController@destroy');      
        Route::post('Pemployees', 'EmployeeController@paginate'); 
    });
       
});
